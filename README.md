# Vivino Integrator
Adds Vivino properties to any wine list in CSV format.

  

---

  

### Installation

* Clone this repo
* `python3 -m venv env`
* `source env/bin/activate`
* `pip install -r requirements.txt`

---

### Usage

* Add wine list in project directory as CSV file.
* Change `INPUT_CSV_NAME` in `main.py` to wine list file name.
* First row in CSV should be the heading.
* First cell of each row is the wine name.

Run `python3 main.py` to create a new CSV that includes Vivino properties. 