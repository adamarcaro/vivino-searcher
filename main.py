import requests
import json
import csv

VIVINO_URL = "https://www.vivino.com/search/wines?q="
ENABLE_LOGS = True
PRINT_ALL_RESULTS = False
INPUT_CSV_NAME = "saq_export.csv"
OUTPUT_CSV_NAME = "output.csv"

def get_rating(wine):
    return wine["aggregateRating"]["ratingValue"]

def get_manufacturer_name(wine):
    return wine["manufacturer"]["name"]

def get_wine_name(wine):
    return wine["name"]

def get_review_count(wine):
    return wine["aggregateRating"]["reviewCount"]

# Gets properties from `wine` and prints them.
def print_wine(wine):
    print("Manufacturer Name: " + get_manufacturer_name(wine))
    print("Wine name: " + get_wine_name(wine))
    print("Rating: " + str(get_rating(wine)))
    print("Review count: " + str(get_review_count(wine)))
    print("\n")

# Searches `search_query` in Vivino and return wine results.
def search_vivino(search_query):
    # Search Vivino for `wine_name`.
    # Returns entire document as response.
    request_url = VIVINO_URL + search_query.replace(" ", "+")
    response = requests.get(request_url, headers={
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0"
    })

    # Parse response and get JSON array of wine results.
    response_str = response.content.decode('utf-8')
    search_results_str = response_str.split(r"<script type='application/ld+json'>")[1].split(r"</script>")[0]
    return json.loads(search_results_str)

# Iterates through CSV file at `input_csv` and returns array of rows
# with Vivino properties added
def get_rows_with_properties(input_csv):
    saq_export = open(input_csv)
    with saq_export:
        # Get heading to initialize new rows.
        rows_with_properties = []

        # Iterate through CSV rows.
        reader = csv.reader(saq_export)
        count = 0
        max = 0
        for row in reader:
            if (ENABLE_LOGS):
                print('Searching for "' + row[0] + '"...')
            if (count >= max & max > 0):
                break
            # First row is heading, copy and add Vivino property names.
            if (count == 0):
                row.append("Rating")
                row.append("Review count")
                row.append("Vivino manufacturer")
                row.append("Vivino name")
                rows_with_properties.append(row)
            else:
                # Try searching Vivino for current wine. May fail if reached API limit.
                try:
                    vivino_match = search_vivino(row[0])[0]
                except:
                    print("Reached Vivino search limit.\n")
                    return rows_with_properties
                # Get Vivino rating. If the rating is 0, it was not found so we ignore.
                try:
                    rating = get_rating(vivino_match)
                    if (rating > 0):
                        row.append(rating)
                        row.append(get_review_count(vivino_match))
                        row.append(get_manufacturer_name(vivino_match))
                        row.append(get_wine_name(vivino_match))
                        rows_with_properties.append(row)
                        if (ENABLE_LOGS):
                            print('Closest Vivino match: "' + get_wine_name(vivino_match) + '".')
                            print('Saved row.\n')
                    else:
                        if (ENABLE_LOGS):
                            print("No results, skipping.\n")
                except:
                    print("No results, skipping.\n")
            count += 1
    
    return rows_with_properties

# Writes a new CSV file for a given set of `rows`.
def write_new_csv(rows):
    if (ENABLE_LOGS):
        print("Creating output file...")
    output = open(OUTPUT_CSV_NAME, "x")
    writer = csv.writer(output)
    writer.writerows(rows)
    if (ENABLE_LOGS):
        print("Successful run. See output in " + OUTPUT_CSV_NAME)

rows = get_rows_with_properties(INPUT_CSV_NAME)
write_new_csv(rows)